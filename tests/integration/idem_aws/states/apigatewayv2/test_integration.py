import copy
import time
from collections import ChainMap

import pytest
import pytest_asyncio


PARAMETRIZE = dict(argnames="__test", argvalues=[True, False], ids=["--test", "run"])
PARAMETER = {
    "name": "idem-test-integration-" + str(int(time.time())),
    "integration_type": "HTTP",
    "connection_type": "INTERNET",
    "content_handling_strategy": "CONVERT_TO_TEXT",
    "integration_method": "GET",
    "integration_uri": "http://idem.example.com",
    "passthrough_behavior": "WHEN_NO_MATCH",
    "payload_format_version": "1.0",
    "request_parameters": {
        "integration.request.header.authToken": "route.request.querystring.authToken"
    },
    "request_templates": {"application/json": '{"statusCode":200}'},
    "template_selection_expression": "expression",
    "timeout_in_millis": 60,
}


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(pro=True)
@pytest.mark.dependency(name="present")
async def test_present(hub, ctx, __test, aws_apigatewayv2_api, cleanup):
    global PARAMETER
    ctx["test"] = __test
    PARAMETER["api_id"] = aws_apigatewayv2_api.get("api_id")
    PARAMETER["description"] = "Idem integration test - " + PARAMETER["name"]

    ret = await hub.states.aws.apigatewayv2.integration.present(ctx, **PARAMETER)
    assert ret["result"], ret["comment"]
    resource = ret["new_state"]
    if __test:
        assert (
            f"Would create aws.apigatewayv2.integration '{PARAMETER['name']}'"
            in ret["comment"]
        )
    else:
        PARAMETER["resource_id"] = resource["resource_id"]
        assert (
            f"Created aws.apigatewayv2.integration '{PARAMETER['name']}'"
            in ret["comment"]
        )
    assert not ret["old_state"] and ret["new_state"]
    assert PARAMETER["name"] == resource.get("name")
    assert PARAMETER["api_id"] == resource.get("api_id")
    assert PARAMETER["integration_type"] == resource.get("integration_type")
    assert PARAMETER["connection_type"] == resource.get("connection_type")
    assert PARAMETER["content_handling_strategy"] == resource.get(
        "content_handling_strategy"
    )
    assert PARAMETER["description"] == resource.get("description")
    assert PARAMETER["integration_method"] == resource.get("integration_method")
    assert PARAMETER["integration_uri"] == resource.get("integration_uri")
    assert PARAMETER["passthrough_behavior"] == resource.get("passthrough_behavior")
    assert PARAMETER["payload_format_version"] == resource.get("payload_format_version")
    assert PARAMETER["request_parameters"] == resource.get("request_parameters")
    assert PARAMETER["request_templates"] == resource.get("request_templates")
    assert PARAMETER["template_selection_expression"] == resource.get(
        "template_selection_expression"
    )
    assert PARAMETER["timeout_in_millis"] == resource.get("timeout_in_millis")


@pytest.mark.asyncio
@pytest.mark.localstack(pro=True)
@pytest.mark.dependency(name="describe", depends=["present"])
async def test_describe(hub, ctx):
    describe_ret = await hub.states.aws.apigatewayv2.integration.describe(ctx)
    resource_id = PARAMETER["resource_id"]
    assert resource_id in describe_ret
    # Verify that the describe output format is correct
    assert "aws.apigatewayv2.integration.present" in describe_ret[resource_id]
    described_resource = describe_ret[resource_id][
        "aws.apigatewayv2.integration.present"
    ]
    resource_map = dict(ChainMap(*described_resource))
    assert resource_id == resource_map.get("resource_id")
    assert resource_id == resource_map.get("name")
    assert PARAMETER["integration_type"] == resource_map.get("integration_type")
    assert PARAMETER["connection_type"] == resource_map.get("connection_type")
    assert PARAMETER["content_handling_strategy"] == resource_map.get(
        "content_handling_strategy"
    )
    assert PARAMETER["description"] == resource_map.get("description")
    assert PARAMETER["integration_method"] == resource_map.get("integration_method")
    assert PARAMETER["integration_uri"] == resource_map.get("integration_uri")
    assert PARAMETER["passthrough_behavior"] == resource_map.get("passthrough_behavior")
    assert PARAMETER["payload_format_version"] == resource_map.get(
        "payload_format_version"
    )
    assert PARAMETER["request_parameters"] == resource_map.get("request_parameters")
    assert PARAMETER["request_templates"] == resource_map.get("request_templates")
    assert PARAMETER["template_selection_expression"] == resource_map.get(
        "template_selection_expression"
    )
    assert PARAMETER["timeout_in_millis"] == resource_map.get("timeout_in_millis")


@pytest.mark.asyncio
@pytest.mark.localstack(pro=True)
@pytest.mark.dependency(name="exec-get", depends=["present"])
# This test is here to avoid the need to create an integration fixture for exec.get() testing
async def test_exec_get(hub, ctx):
    ret = await hub.exec.aws.apigatewayv2.integration.get(
        ctx=ctx,
        name=PARAMETER["name"],
        resource_id=PARAMETER["resource_id"],
        api_id=PARAMETER["api_id"],
    )
    assert ret["result"], ret["comment"]
    assert ret["ret"]
    resource = ret["ret"]
    assert PARAMETER["name"] == resource.get("name")
    assert PARAMETER["resource_id"] == resource.get("resource_id")
    assert PARAMETER["integration_type"] == resource.get("integration_type")
    assert PARAMETER["connection_type"] == resource.get("connection_type")
    assert PARAMETER["content_handling_strategy"] == resource.get(
        "content_handling_strategy"
    )
    assert PARAMETER["description"] == resource.get("description")
    assert PARAMETER["integration_method"] == resource.get("integration_method")
    assert PARAMETER["integration_uri"] == resource.get("integration_uri")
    assert PARAMETER["passthrough_behavior"] == resource.get("passthrough_behavior")
    assert PARAMETER["payload_format_version"] == resource.get("payload_format_version")
    assert PARAMETER["request_parameters"] == resource.get("request_parameters")
    assert PARAMETER["request_templates"] == resource.get("request_templates")
    assert PARAMETER["template_selection_expression"] == resource.get(
        "template_selection_expression"
    )
    assert PARAMETER["timeout_in_millis"] == resource.get("timeout_in_millis")


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(pro=True)
@pytest.mark.dependency(name="update", depends=["describe"])
async def test_update(hub, ctx, __test):
    global PARAMETER
    new_parameter = copy.deepcopy(PARAMETER)
    ctx["test"] = __test
    new_parameter["content_handling_strategy"] = "CONVERT_TO_BINARY"
    new_parameter["description"] = "Idem integration test - new - " + PARAMETER["name"]
    new_parameter["integration_method"] = "POST"
    new_parameter["integration_uri"] = "http://new-idem.example.com"
    new_parameter["passthrough_behavior"] = "NEVER"
    new_parameter["request_parameters"] = {
        "integration.request.header.authToken": "route.request.querystring.authToken"
    }
    new_parameter["request_templates"] = {"application/json": '{"statusCode":201}'}
    new_parameter["template_selection_expression"] = "new_expression"
    new_parameter["timeout_in_millis"] = 120

    ret = await hub.states.aws.apigatewayv2.integration.present(ctx, **new_parameter)
    assert ret["result"], ret["comment"]
    assert ret["old_state"] and ret["new_state"]
    old_resource = ret["old_state"]
    assert PARAMETER["name"] == old_resource.get("name")
    assert PARAMETER["resource_id"] == old_resource.get("resource_id")
    assert PARAMETER["integration_type"] == old_resource.get("integration_type")
    assert PARAMETER["connection_type"] == old_resource.get("connection_type")
    assert PARAMETER["content_handling_strategy"] == old_resource.get(
        "content_handling_strategy"
    )
    assert PARAMETER["description"] == old_resource.get("description")
    assert PARAMETER["integration_method"] == old_resource.get("integration_method")
    assert PARAMETER["integration_uri"] == old_resource.get("integration_uri")
    assert PARAMETER["passthrough_behavior"] == old_resource.get("passthrough_behavior")
    assert PARAMETER["payload_format_version"] == old_resource.get(
        "payload_format_version"
    )
    assert PARAMETER["request_parameters"] == old_resource.get("request_parameters")
    assert PARAMETER["request_templates"] == old_resource.get("request_templates")
    assert PARAMETER["template_selection_expression"] == old_resource.get(
        "template_selection_expression"
    )
    assert PARAMETER["timeout_in_millis"] == old_resource.get("timeout_in_millis")
    resource = ret["new_state"]
    assert new_parameter["name"] == resource.get("name")
    assert new_parameter["resource_id"] == resource.get("resource_id")
    assert new_parameter["integration_type"] == resource.get("integration_type")
    assert new_parameter["connection_type"] == resource.get("connection_type")
    assert new_parameter["content_handling_strategy"] == resource.get(
        "content_handling_strategy"
    )
    assert new_parameter["description"] == resource.get("description")
    assert new_parameter["integration_method"] == resource.get("integration_method")
    assert new_parameter["integration_uri"] == resource.get("integration_uri")
    assert new_parameter["passthrough_behavior"] == resource.get("passthrough_behavior")
    assert new_parameter["payload_format_version"] == resource.get(
        "payload_format_version"
    )
    assert new_parameter["request_parameters"] == resource.get("request_parameters")
    assert new_parameter["request_templates"] == resource.get("request_templates")
    assert new_parameter["template_selection_expression"] == resource.get(
        "template_selection_expression"
    )
    assert new_parameter["timeout_in_millis"] == resource.get("timeout_in_millis")
    assert (
        hub.tool.aws.comment_utils.already_exists_comment(
            resource_type="aws.apigatewayv2.integration",
            name=PARAMETER["name"],
        )[0]
        in ret["comment"]
    )
    if __test:
        assert (
            f"Would update parameters: content_handling_strategy,description,integration_method,integration_uri,"
            f"passthrough_behavior,request_templates,template_selection_expression,timeout_in_millis"
            in ret["comment"]
        )
    else:
        assert (
            f"Updated parameters: content_handling_strategy,description,integration_method,integration_uri,"
            f"passthrough_behavior,request_templates,template_selection_expression,timeout_in_millis"
            in ret["comment"]
        )
        PARAMETER = new_parameter


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(pro=True)
@pytest.mark.dependency(name="absent", depends=["update"])
async def test_absent(hub, ctx, __test):
    ctx["test"] = __test
    ret = await hub.states.aws.apigatewayv2.integration.absent(
        ctx,
        name=PARAMETER["name"],
        api_id=PARAMETER["api_id"],
        resource_id=PARAMETER["resource_id"],
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"] and not ret["new_state"]
    old_resource = ret["old_state"]
    assert PARAMETER["name"] == old_resource.get("name")
    assert PARAMETER["resource_id"] == old_resource.get("resource_id")
    assert PARAMETER["integration_type"] == old_resource.get("integration_type")
    assert PARAMETER["connection_type"] == old_resource.get("connection_type")
    assert PARAMETER["content_handling_strategy"] == old_resource.get(
        "content_handling_strategy"
    )
    assert PARAMETER["description"] == old_resource.get("description")
    assert PARAMETER["integration_method"] == old_resource.get("integration_method")
    assert PARAMETER["integration_uri"] == old_resource.get("integration_uri")
    assert PARAMETER["passthrough_behavior"] == old_resource.get("passthrough_behavior")
    assert PARAMETER["payload_format_version"] == old_resource.get(
        "payload_format_version"
    )
    assert PARAMETER["request_parameters"] == old_resource.get("request_parameters")
    assert PARAMETER["request_templates"] == old_resource.get("request_templates")
    assert PARAMETER["template_selection_expression"] == old_resource.get(
        "template_selection_expression"
    )
    assert PARAMETER["timeout_in_millis"] == old_resource.get("timeout_in_millis")
    if __test:
        assert (
            hub.tool.aws.comment_utils.would_delete_comment(
                resource_type="aws.apigatewayv2.integration", name=PARAMETER["name"]
            )[0]
            in ret["comment"]
        )
    else:
        assert (
            hub.tool.aws.comment_utils.delete_comment(
                resource_type="aws.apigatewayv2.integration", name=PARAMETER["name"]
            )[0]
            in ret["comment"]
        )


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(pro=True)
@pytest.mark.dependency(name="already_absent", depends=["absent"])
async def test_already_absent(hub, ctx, __test):
    ctx["test"] = __test
    ret = await hub.states.aws.apigatewayv2.integration.absent(
        ctx,
        name=PARAMETER["name"],
        api_id=PARAMETER["api_id"],
        resource_id=PARAMETER["resource_id"],
    )
    assert ret["result"], ret["comment"]
    assert (not ret["old_state"]) and (not ret["new_state"])
    assert (
        hub.tool.aws.comment_utils.already_absent_comment(
            resource_type="aws.apigatewayv2.integration", name=PARAMETER["name"]
        )[0]
        in ret["comment"]
    )
    if not __test:
        PARAMETER.pop("resource_id")


@pytest.mark.asyncio
@pytest.mark.dependency(depends=["present"])
async def test_absent_with_none_resource_id(hub, ctx):
    # Delete apigatewayv2 integration with no resource_id will consider it as absent
    ret = await hub.states.aws.apigatewayv2.integration.absent(
        ctx, name=PARAMETER["name"], api_id=PARAMETER["api_id"], resource_id=None
    )
    assert ret["result"], ret["comment"]
    assert (not ret["old_state"]) and (not ret["new_state"])
    assert (
        hub.tool.aws.comment_utils.already_absent_comment(
            resource_type="aws.apigatewayv2.integration", name=PARAMETER["name"]
        )[0]
        in ret["comment"]
    )


# This cleanup fixture cleans up the resource after all tests have run
@pytest_asyncio.fixture(scope="module")
async def cleanup(hub, ctx):
    global PARAMETER
    yield None
    if "resource_id" in PARAMETER:
        ret = await hub.states.aws.apigatewayv2.integration.absent(
            ctx,
            name=PARAMETER["name"],
            resource_id=PARAMETER["resource_id"],
        )
        assert ret["result"], ret["comment"]
