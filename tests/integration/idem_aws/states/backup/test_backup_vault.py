import time
from collections import ChainMap

import pytest

PARAMETRIZE = dict(argnames="__test", argvalues=[True, False], ids=["--test", "run"])
PARAMETER = {
    "name": "idem_backup_vault",
    "encryption_key_arn": "arn:aws:kms:us-east-1:011922870716:key/ff4fd718-612c-4d0f-8172-8150025bb4fa",
}


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="present")
@pytest.mark.localstack(False)
async def test_present(hub, ctx, __test, aws_kms_key):
    global PARAMETER
    PARAMETER["name"] = "idem_backup_vault" + str(int(time.time()))
    PARAMETER["encryption_key_arn"] = aws_kms_key["arn"]
    PARAMETER["tags"] = {"Key": "resource", "Value": "backup_vault"}
    ctx["test"] = __test
    ret = await hub.states.aws.backup.backup_vault.present(
        ctx,
        **PARAMETER,
    )

    assert ret["result"], ret["comment"]
    resource = ret["new_state"]
    if __test:
        assert (
            hub.tool.aws.comment_utils.would_create_comment(
                resource_type="aws.backup.backup_vault",
                name=PARAMETER["name"],
            )
            == ret["comment"]
        )
    else:
        PARAMETER["resource_id"] = resource["resource_id"]
        assert (
            hub.tool.aws.comment_utils.create_comment(
                resource_type="aws.backup.backup_vault",
                name=PARAMETER["name"],
            )[0]
            in ret["comment"]
        )
    assert not ret["old_state"] and ret["new_state"]
    assert_backup_vault(hub, ctx, resource, PARAMETER)


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="update", depends=["present"])
@pytest.mark.localstack(False)
async def test_update(hub, ctx, __test):
    global PARAMETER
    PARAMETER["tags"] = {"Key": "resource", "Value": "backup_vault_updated"}
    ret = await hub.states.aws.backup.backup_vault.present(
        ctx,
        **PARAMETER,
    )

    assert ret["result"], ret["comment"]
    resource = ret["new_state"]
    PARAMETER["resource_id"] = resource["resource_id"]
    assert ret["old_state"] and ret["new_state"]
    assert_backup_vault(hub, ctx, resource, PARAMETER)


@pytest.mark.asyncio
@pytest.mark.dependency(name="describe", depends=["update"])
@pytest.mark.localstack(False)
async def test_describe(hub, ctx):
    describe_ret = await hub.states.aws.backup.backup_vault.describe(ctx)
    # Verify that describe output format is correct
    assert "aws.backup.backup_vault.present" in describe_ret.get(
        PARAMETER["resource_id"]
    )
    describe_params = describe_ret.get(PARAMETER["resource_id"]).get(
        "aws.backup.backup_vault.present"
    )
    described_resource_map = dict(ChainMap(*describe_params))
    assert_backup_vault(hub, ctx, described_resource_map, PARAMETER)


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="test_exec_get", depends=["describe"])
@pytest.mark.localstack(False)
async def test_exec_get(hub, ctx, __test):
    global PARAMETER
    ret = await hub.exec.aws.backup.backup_vault.get(
        ctx=ctx, name=PARAMETER["name"], resource_id=PARAMETER["resource_id"]
    )
    assert ret["result"], ret["comment"]
    assert ret["ret"]
    assert_backup_vault(hub, ctx, ret["ret"], PARAMETER)


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="absent", depends=["present"])
@pytest.mark.localstack(False)
async def test_absent(hub, ctx, __test):
    """
    Destroy the backup vault by the present state
    """
    ctx["test"] = __test
    ret = await hub.states.aws.backup.backup_vault.absent(
        ctx,
        name=PARAMETER["name"],
        resource_id=PARAMETER["resource_id"],
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"] and not ret["new_state"]
    assert_backup_vault(hub, ctx, ret["old_state"], PARAMETER)
    if __test:
        assert (
            hub.tool.aws.comment_utils.would_delete_comment(
                resource_type="aws.backup.backup_vault",
                name=PARAMETER["name"],
            )[0]
            in ret["comment"]
        )
    else:
        assert (
            hub.tool.aws.comment_utils.delete_comment(
                resource_type="aws.backup.backup_vault",
                name=PARAMETER["name"],
            )[0]
            in ret["comment"]
        )


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="already_absent", depends=["absent"])
@pytest.mark.localstack(False)
async def test_already_absent(hub, ctx, __test):
    ctx["test"] = __test
    ret = await hub.states.aws.backup.backup_vault.absent(
        ctx,
        name=PARAMETER["resource_id"],
    )
    assert ret["result"], ret["comment"]
    assert (not ret["old_state"]) and (not ret["new_state"])
    assert (
        hub.tool.aws.comment_utils.already_absent_comment(
            resource_type="aws.backup.backup_vault",
            name=PARAMETER["name"],
        )[0]
        in ret["comment"]
    )
    if not __test:
        PARAMETER.pop("resource_id")


def assert_backup_vault(hub, ctx, resource, parameters):
    assert parameters.get("name") == resource.get("name")
    assert parameters.get("encryption_key_arn") == resource.get("encryption_key_arn")
    assert parameters.get("name") == resource.get("name")
    assert parameters.get("tags") == resource.get("tags")
