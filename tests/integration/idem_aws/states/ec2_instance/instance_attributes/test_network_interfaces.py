import copy
from typing import Dict
from typing import List

import pytest


@pytest.fixture(autouse=True, scope="module")
def skip_on_real_aws(ctx):
    if not ctx.acct.get("endpoint_url"):
        raise pytest.skip("This test only runs on localstack")


@pytest.mark.asyncio
@pytest.mark.localstack(pro=True)
@pytest.mark.dependency(name="create")
async def test_create(hub, ctx, aws_ec2_instance, aws_ec2_network_interface, __test):
    """
    Attach new network interface
    """
    state = copy.copy(aws_ec2_instance)
    state["network_interfaces"] = [
        {
            "network_interface_id": aws_ec2_network_interface.resource_id,
            "device_index": 1,
            "network_card_index": 1,
        }
    ]

    # Run the present state with a modified value
    ret = await hub.states.aws.ec2.instance.present(ctx, **state)
    assert ret["result"], ret["comment"]

    # Before creation, report changes that would be made
    if __test <= 1:
        assert not _get_network_interface(
            aws_ec2_network_interface.resource_id,
            ret["changes"]["old"].get("network_interfaces", []),
        )
        assert _get_network_interface(
            aws_ec2_network_interface.resource_id,
            ret["changes"]["new"].get("network_interfaces", []),
        )
    # After creation, no changes made
    else:
        assert not ret["changes"] is None, ret["changes"]


@pytest.mark.asyncio
@pytest.mark.localstack(pro=True)
# @pytest.mark.dependency(depends=["present"])
async def test_update(hub, ctx, aws_ec2_instance, aws_ec2_network_interface, __test):
    """
    Move network interface
    """
    state = copy.copy(aws_ec2_instance)
    # Use a list dict this time for tags
    state["network_interfaces"] = [
        {
            "network_interface_id": aws_ec2_network_interface.resource_id,
            "device_index": 2,
            "network_card_index": 3,
        }
    ]

    # Run the present state with a modified value
    ret = await hub.states.aws.ec2.instance.present(ctx, **state)
    assert ret["result"], ret["comment"]

    # Before creation, report changes that would be made
    if __test <= 1:
        old_ni = _get_network_interface(
            aws_ec2_network_interface.resource_id,
            ret["changes"]["old"]["network_interfaces"],
        )
        new_ni = _get_network_interface(
            aws_ec2_network_interface.resource_id,
            ret["changes"]["new"]["network_interfaces"],
        )
        assert old_ni["device_index"] == 1
        assert new_ni["device_index"] == 2
    # After creation, no changes made
    else:
        assert not ret["changes"] is None, ret["changes"]


@pytest.mark.asyncio
@pytest.mark.localstack(pro=True)
async def test_delete(hub, ctx, aws_ec2_instance, aws_ec2_network_interface, __test):
    """
    Detach network interface
    """
    state = copy.copy(aws_ec2_instance)

    # Run the present state with a modified value, it should detach the network interface
    ret = await hub.states.aws.ec2.instance.present(ctx, **state)
    assert ret["result"], ret["comment"]


def _get_network_interface(
    search_id: str, interfaces: List[Dict[str, str]]
) -> Dict[str, str]:
    for interface in interfaces:
        if search_id == interface.get("network_interface_id"):
            return interface
    return {}
