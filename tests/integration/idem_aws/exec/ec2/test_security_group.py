import time
from typing import List

import pytest


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
async def test_get(hub, ctx, aws_ec2_security_group):
    security_group_get_name = "idem-test-exec-get-security_group-" + str(
        int(time.time())
    )
    ret = await hub.exec.aws.ec2.security_group.get(
        ctx,
        name=security_group_get_name,
        resource_id=aws_ec2_security_group["resource_id"],
    )
    assert ret["result"], ret["comment"]
    assert ret["ret"]
    resource = ret["ret"]
    assert aws_ec2_security_group["resource_id"] == resource.get("resource_id")


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
async def test_get_filter(hub, ctx, aws_ec2_security_group):
    security_group_get_name = "idem-test-exec-get-security_group-" + str(
        int(time.time())
    )
    ret = await hub.exec.aws.ec2.security_group.get(
        ctx,
        name=security_group_get_name,
        filters=[{"name": "description", "values": ["*fixture*"]}],
    )
    assert ret["result"], ret["comment"]
    assert "ret" in ret
    resource = ret["ret"]
    assert aws_ec2_security_group["resource_id"] == resource.get("resource_id")


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
async def test_get_invalid_resource_id(hub, ctx):
    security_group_get_name = "idem-test-exec-get-security_group-" + str(
        int(time.time())
    )
    ret = await hub.exec.aws.ec2.security_group.get(
        ctx,
        name=security_group_get_name,
        resource_id="fake-id",
    )
    assert ret["result"] is False
    assert ret["comment"]
    assert ret["ret"] is None
    assert (
        f"ClientError: An error occurred (InvalidGroup.NotFound) when calling the DescribeSecurityGroups operation: The security group '{{'fake-id'}}' does not exist"
        in str(ret["comment"])
    )


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
async def test_list(hub, ctx, aws_ec2_security_group):
    security_group_list_name = "idem-test-exec-list-security_group-" + str(
        int(time.time())
    )
    filters = [{"name": "group-id", "values": [aws_ec2_security_group["resource_id"]]}]
    ret = await hub.exec.aws.ec2.security_group.list(
        ctx,
        name=security_group_list_name,
        filters=filters,
    )
    assert ret["result"], ret["comment"]
    assert isinstance(ret["ret"], List)
    resource = {}
    for entry in ret["ret"]:
        if entry["resource_id"] == aws_ec2_security_group["resource_id"]:
            resource = entry
    assert aws_ec2_security_group["resource_id"] == resource.get("resource_id")
