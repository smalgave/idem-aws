import time
from typing import List

import pytest


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
async def test_get(hub, ctx, aws_ec2_vpc):
    vpc_get_name = "idem-test-exec-get-vpc-" + str(int(time.time()))
    vpc_fixture = await hub.tool.aws.ec2.conversion_utils.convert_raw_vpc_to_present(
        ctx=ctx, raw_resource=aws_ec2_vpc, idem_resource_name="idem-fixture-vpc"
    )
    ret = await hub.exec.aws.ec2.vpc.get(
        ctx,
        name=vpc_get_name,
        resource_id=vpc_fixture["resource_id"],
    )
    assert ret["result"], ret["comment"]
    assert ret["ret"]
    resource = ret["ret"]
    assert vpc_get_name == resource.get("name")
    assert vpc_fixture["resource_id"] == resource.get("resource_id")


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
async def test_get_invalid_resource_id(hub, ctx):
    vpc_get_name = "idem-test-exec-get-vpc-" + str(int(time.time()))
    ret = await hub.exec.aws.ec2.vpc.get(
        ctx,
        name=vpc_get_name,
        resource_id="fake-id",
    )
    assert ret["result"], ret["comment"]
    assert ret["ret"] is None
    assert f"Get aws.ec2.vpc '{vpc_get_name}' result is empty" in str(ret["comment"])


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
async def test_list(hub, ctx, aws_ec2_vpc):
    vpc_list_name = "idem-test-exec-list-vpc-" + str(int(time.time()))
    vpc_fixture = await hub.tool.aws.ec2.conversion_utils.convert_raw_vpc_to_present(
        ctx=ctx, raw_resource=aws_ec2_vpc, idem_resource_name="idem-fixture-vpc"
    )
    filters = [{"name": "vpc-id", "values": [vpc_fixture["resource_id"]]}]
    ret = await hub.exec.aws.ec2.vpc.list(
        ctx,
        name=vpc_list_name,
        filters=filters,
    )
    assert ret["result"], ret["comment"]
    assert isinstance(ret["ret"], List)
    assert len(ret["ret"]) == 1
    resource = ret["ret"][0]
    assert vpc_fixture["resource_id"] == resource.get("resource_id")


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
async def test_list_with_default(hub, ctx, aws_ec2_vpc):
    vpc_list_name = "idem-test-exec-list-vpc-" + str(int(time.time()))
    vpc_fixture = await hub.tool.aws.ec2.conversion_utils.convert_raw_vpc_to_present(
        ctx=ctx, raw_resource=aws_ec2_vpc, idem_resource_name="idem-fixture-vpc"
    )
    ret = await hub.exec.aws.ec2.vpc.list(
        ctx,
        name=vpc_list_name,
        default=True,
    )
    assert ret["result"], ret["comment"]
    assert isinstance(ret["ret"], List)
    assert len(ret["ret"]) == 0
