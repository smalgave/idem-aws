import copy
import uuid

import pytest


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
async def test_update_status(hub, ctx):
    if hub.tool.utils.is_running_localstack(ctx):
        return
    valid_cost_allocation_tags = {"alarms": "Active"}
    valid_cost_allocation_tags_message = [{"TagKey": "alarms", "Status": "Active"}]
    random_tag_name = "idem-test-" + str(uuid.uuid4())
    random_valid_cost_allocation_tags = {random_tag_name: "Active"}

    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    # Update Cost allocation tags with Test Flag
    ret = await hub.exec.aws.costexplorer.cost_allocation_tag.update_status(
        test_ctx,
        tags=valid_cost_allocation_tags,
    )

    assert ret["result"], ret["comment"]
    assert (
        f"Would update aws.costexplorer.cost_allocation_tag '{valid_cost_allocation_tags_message}'"
        in ret["comment"]
    )
    assert valid_cost_allocation_tags_message == ret["ret"]["cost_allocation_tags"]

    # Update Cost allocation tags in real
    ret = await hub.exec.aws.costexplorer.cost_allocation_tag.update_status(
        ctx,
        tags=valid_cost_allocation_tags,
    )
    assert ret["result"], ret["comment"]
    assert (
        f"Updated aws.costexplorer.cost_allocation_tag '{valid_cost_allocation_tags_message}'"
        in ret["comment"]
    )
    assert valid_cost_allocation_tags_message == ret["ret"]["cost_allocation_tags"]

    ret = await hub.exec.aws.costexplorer.cost_allocation_tag.update_status(
        ctx,
        tags=random_valid_cost_allocation_tags,
    )
    assert not ret["result"], ret["comment"]
    assert not ret["ret"]
