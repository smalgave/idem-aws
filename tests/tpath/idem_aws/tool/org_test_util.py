import re

ORGANIZATION_ARN_FORMAT = "arn:aws:organizations::{0}:organization/{1}"
MASTER_ACCOUNT_ARN_FORMAT = "arn:aws:organizations::{0}:account/{1}/{0}"
ACCOUNT_ARN_FORMAT = "arn:aws:organizations::{0}:account/{1}/{2}"

ORG_ID_SIZE = 10
ORG_ID_REGEX = r"o-[a-z0-9]{%s}" % ORG_ID_SIZE


def validate_organization(hub, response):
    org = response
    assert sorted(org.keys()) == (
        [
            "arn",
            "available_policy_types",
            "feature_set",
            "master_account_arn",
            "master_account_email",
            "master_account_id",
            "name",
            "resource_id",
            "roots",
        ]
    )

    assert re.compile(ORG_ID_REGEX).match(org["resource_id"])
    assert org["master_account_arn"] == MASTER_ACCOUNT_ARN_FORMAT.format(
        org["master_account_id"], org["resource_id"]
    )
    assert org["arn"] == ORGANIZATION_ARN_FORMAT.format(
        org["master_account_id"], org["resource_id"]
    )
    assert org["feature_set"] in ["ALL", "CONSOLIDATED_BILLING"]
    assert org["available_policy_types"] == [
        {"Type": "SERVICE_CONTROL_POLICY", "Status": "ENABLED"}
    ]
