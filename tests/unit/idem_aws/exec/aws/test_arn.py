def test_build(hub, ctx):
    ret = hub.exec.aws.arn.build(
        ctx,
        partition="fakepartition",
        service="fakeservice",
        region="fakeregion",
        account_id="fakeaccountid",
        resource="fakeresource",
    )
    assert ret["result"], ret["comment"]
    assert (
        "arn:fakepartition:fakeservice:fakeregion:fakeaccountid:fakeresource"
        == ret["ret"]
    )

    ret = hub.exec.aws.arn.build(
        ctx,
        service="fakeservice",
        resource="fakeresource",
    )
    assert ret["result"], ret["comment"]
    assert "arn:aws:fakeservice:::fakeresource" == ret["ret"]


def test_parse(hub, ctx):
    ret = hub.exec.aws.arn.parse(
        ctx, "arn:fakepartition:fakeservice:fakeregion:fakeaccountid:fakeresource"
    )
    assert ret["result"], ret["comment"]
    parts = ret["ret"]
    assert "fakepartition" == parts["partition"]
    assert "fakeservice" == parts["service"]
    assert "fakeregion" == parts["region"]
    assert "fakeaccountid" == parts["account_id"]
    assert "fakeresource" == parts["resource"]

    ret = hub.exec.aws.arn.parse(
        ctx, "arn:fakepartition:fakeservice:fakeresion:fakeaccountid"
    )
    assert not ret["result"] and ret["comment"]
    assert "Invalid AWS ARN" in ret["comment"][0]
