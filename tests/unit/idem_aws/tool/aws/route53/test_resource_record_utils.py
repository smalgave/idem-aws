def test_same_states(hub):
    state1 = {
        "name": "www.example.com.",
        "resource_id": "D7TI3LJM4US4BOY_www.example.com_MX",
        "hosted_zone_id": "/hostedzone/D7TI3LJM4US4BOY",
        "record_type": "MX",
        "ttl": 300,
        "resource_records": ["75.11.24.55", "10.31.31.10"],
    }
    state2 = {
        "resource_id": "D7TI3LJM4US4BOY_www.example.com_MX",
        "name": "www.example.com.",
        "hosted_zone_id": "/hostedzone/D7TI3LJM4US4BOY",
        "record_type": "MX",
        "ttl": 300,
        "resource_records": ["10.31.31.10", "75.11.24.55"],
    }

    assert hub.tool.aws.route53.resource_record_utils.same_states(
        state1, state2
    ), "States should be equal"
    assert hub.tool.aws.route53.resource_record_utils.same_states(
        None, None
    ), "States should be equal"
    assert not hub.tool.aws.route53.resource_record_utils.same_states(
        state1, None
    ), "States should differ"

    state1 = {
        "name": "www.example.com.",
        "resource_id": "D7TI3LJM4US4BOY_www.example.com_MX",
        "hosted_zone_id": "/hostedzone/D7TI3LJM4US4BOY",
        "record_type": "MX",
        "ttl": 300,
        "resource_records": ["75.11.24.55", "10.31.31.10"],
    }
    state2 = {
        "resource_id": "D7TI3LJM4US4BOY_www.example.com_MX",
        "name": "www.example.com.",
        "hosted_zone_id": "/hostedzone/D7TI3LJM4US4BOY",
        "record_type": "MX",
        "ttl": 300,
        "resource_records": ["10.31.31.10", "75.11.24.56"],
    }

    assert not hub.tool.aws.route53.resource_record_utils.same_states(
        state1, state2
    ), "States should differ in resource_records"

    state1 = {
        "name": "www.example.com.",
        "resource_id": "D7TI3LJM4US4BOY_www.example.com_MX",
        "hosted_zone_id": "/hostedzone/D7TI3LJM4US4BOY",
        "record_type": "MX",
        "ttl": 300,
        "resource_records": ["75.11.24.55", "10.31.31.10"],
    }
    state2 = {
        "resource_id": "D7TI3LJM4US4BOY_www.example.com_MX",
        "name": "www.example.com.",
        "hosted_zone_id": "/hostedzone/D7TI3LJM4US4BOY",
        "record_type": "MX",
        "ttl": 300,
    }

    assert not hub.tool.aws.route53.resource_record_utils.same_states(
        state1, state2
    ), "States should differ in resource_records"

    state1 = {
        "name": "www.example.com.",
        "resource_id": "D7TI3LJM4US4BOY_www.example.com_MX",
        "hosted_zone_id": "/hostedzone/D7TI3LJM4US4BOY",
        "record_type": "MX",
        "ttl": 301,
        "resource_records": ["75.11.24.55", "10.31.31.10"],
    }
    state2 = {
        "resource_id": "D7TI3LJM4US4BOY_www.example.com_MX",
        "name": "www.example.com.",
        "hosted_zone_id": "/hostedzone/D7TI3LJM4US4BOY",
        "record_type": "MX",
        "ttl": 300,
        "resource_records": ["10.31.31.10", "75.11.24.55"],
    }

    assert not hub.tool.aws.route53.resource_record_utils.same_states(
        state1, state2
    ), "States should differ in ttl"
