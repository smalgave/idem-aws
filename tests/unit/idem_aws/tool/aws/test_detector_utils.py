def test_compare_data_sources(hub):

    # comparing two dicts and returning true if they are same
    data_sources_old = {"S3Logs": {"Enable": True}}
    data_sources_new = {"S3Logs": {"Enable": True}}
    compare_data_sources = hub.tool.aws.guardduty.detector.compare_data_sources(
        data_sources_old, data_sources_new
    )
    assert compare_data_sources

    # comparing two dicts and returning false if they are not same
    data_sources_old = {"S3Logs": {"Enable": True}}
    data_sources_new = {"S3Logs": {"Enable": False}}
    compare_data_sources = hub.tool.aws.guardduty.detector.compare_data_sources(
        data_sources_old, data_sources_new
    )
    assert not compare_data_sources


def test_render_data_sources(hub):

    # rendering data_sources to make resources similar
    before_render = {"S3Logs": {"Status": "ENABLED"}}
    after_render = {"S3Logs": {"Enable": True}}
    render_result = hub.tool.aws.guardduty.detector.render_data_sources(before_render)
    compare_data_sources = hub.tool.aws.guardduty.detector.compare_data_sources(
        render_result, after_render
    )
    assert compare_data_sources

    before_render = {"Kubernetes": {"AuditLogs": {"Status": "ENABLED"}}}
    after_render = {"Kubernetes": {"AuditLogs": {"Enable": True}}}
    render_result = hub.tool.aws.guardduty.detector.render_data_sources(before_render)
    compare_data_sources = hub.tool.aws.guardduty.detector.compare_data_sources(
        render_result, after_render
    )
    assert compare_data_sources

    before_render = {
        "MalwareProtections": {
            "ScanEc2InstanceWithFindings": {"EbsVolumes": {"Status": "ENABLED"}}
        }
    }
    after_render = {
        "MalwareProtection": {
            "ScanEc2InstanceWithFindings": {"EbsVolumes": {"Enable": True}}
        }
    }
    render_result = hub.tool.aws.guardduty.detector.render_data_sources(before_render)
    compare_data_sources = hub.tool.aws.guardduty.detector.compare_data_sources(
        render_result, after_render
    )
    assert compare_data_sources
