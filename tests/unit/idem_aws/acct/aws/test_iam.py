import configparser
from unittest import mock

import dict_tools.data
import pytest

PROFILE = {
    "aws.iam": {
        "paths": ["./path"],
        "region": "us-east-1",
    }
}


class mock_config_parser(configparser.ConfigParser):
    def read(self, filenames, *args, **kwargs):
        self.read_string(
            "[default]\naws_access_key_id = my_key\naws_secret_access_key = my_secret_key"
        )


@pytest.mark.asyncio
async def test_gather(hub):
    hub.OPT = dict_tools.data.NamespaceDict(acct=dict(extras=PROFILE))

    with mock.patch.object(configparser, "ConfigParser", mock_config_parser):
        p1 = await hub.acct.init.gather(
            subs=["aws"], profile="default", profiles=PROFILE
        )

    assert p1["aws_access_key_id"] == "my_key"
    assert p1["aws_secret_access_key"] == "my_secret_key"
    assert p1["region_name"] == "us-east-1"
    assert "paths" not in p1
